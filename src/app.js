const express = require('express')
const app = express()
const mongoose = require('mongoose')
const PORT = process.env.PORT || 8080
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const authRoutes = require('./routes/auth')
const userRoutes = require('./routes/user')
const loadRoutes = require('./routes/load')
const truckRoutes = require('./routes/truck')
const config = require('../config')
const authMiddleware = require('./middleware/auth')

mongoose.connect(config.mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('MongoDB connected.'))
    .catch(err => console.log(err))

app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use('/api/auth', authRoutes)
app.use('/api/users', [authMiddleware], userRoutes)
app.use('/api/loads', [authMiddleware], loadRoutes)
app.use('/api/trucks', [authMiddleware], truckRoutes)

app.listen(PORT, () => console.log(`Server is running on http://localhost:${PORT}`))