module.exports = (schema, property) => {
    return async (req, res, next) => {
        if (req.method === 'OPTIONS') {
            next()
        }

        try {
            const validation = await schema.validate(req[property])
            if (validation.error) {
                return res.status(400).json({
                    message: validation.error.details[0].message
                });
            }
            next();
        } catch (error) {
            res.status(400).json({
                message: error.details[0].message
            })
        }
    }
}