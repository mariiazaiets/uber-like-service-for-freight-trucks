const Load = require('../models/Load')
const Truck = require('../models/Truck')

module.exports = async function(loadId) {
    try {
        const load = await Load.findById(loadId)
        const {length, height, width} = load.dimensions;
        const truck = await Truck
            .where('assigned_to').ne(null)
            .where('status').equals('IS')
            .where('payload').gt(load.payload)
            .where('width').gt(width)
            .where('length').gt(length)
            .where('height').gt(height)
            .findOne()

        console.log('Looking for truck')

        const updateLogs = [...load.logs]

        if (!truck) {
            updateLogs.push({
                message: 'Status changed to NEW. No suitable truck',
                time: Date.now()
            })

            await Load.findByIdAndUpdate(loadId, {status: 'NEW', logs: updateLogs})

            return {
                state: 'No suitable truck'
            }
        }

        await Truck.findByIdAndUpdate(truck._id, {status: 'OL'})
        console.log('Truck status updated')

        updateLogs.push({
            message: 'Load is assigned. Driver is en route',
            time: Date.now()
        })

        await Load.findByIdAndUpdate(
            loadId,
            {
                status: 'ASSIGNED',
                state: 'En route to Pick Up',
                logs: updateLogs,
                assigned_to: truck.assigned_to
            }
        )

        console.log('Load is en route')
        return {
            state: 'En route to Pick Up'
        }
    } catch (err) {
        return {
            message: err
        }
    }
}