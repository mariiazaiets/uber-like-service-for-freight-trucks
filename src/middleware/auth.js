const jwt = require('jsonwebtoken')
const config = require('../../config')

module.exports = (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return next()
    }

    try {
        const {
            authorization,
        } = req.headers;

        if (!authorization) {
            return res.status(400).json({
                message: 'Please, provide "authorization" header'
            });
        }

        const [, token] = authorization.split(' ');
        if (!token) {
            return res.status(400).json({
                message: 'Please, include token to request'
            });
        }

        req.user = jwt.verify(token, config.jwt);
        next();
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}