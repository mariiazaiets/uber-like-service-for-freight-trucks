const bcrypt = require('bcrypt')
const User = require('../models/User')
const Truck = require('../models/Truck')

module.exports.getProfileInfo = async function (req, res) {
    try {
        const userId = req.user.userId;
        const user = await User.findById(userId);

        if (!user) {
            return res.status(400).json({
                message: 'User is not found'
            })
        }

        res.status(200).json({
            user: {
                _id: userId,
                role: user.role,
                email: user.email,
                created_date: user['createdAt'],
            },
        });
    } catch (err) {
        res.status(400).send({
            message: `Error: ${err}`,
        });
    }

}

module.exports.deleteUserProfile = async function (req, res) {
    try {
        const userId = req.user.userId
        const onLoadTruck = await Truck.findOne(
            {created_by: userId, status: 'OL'},
        );

        if (onLoadTruck) {
            return res.status(400).json({
                message: 'It is forbidden to delete this account while the driver is loaded'
            });
        }

        await User.findByIdAndDelete(userId);
        await Truck.deleteMany({created_by: userId});

        res.status(200).json({
            message: 'Profile deleted successfully',
        });
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }

}

module.exports.changeUserPassword = async function (req, res) {
    try {
        const {oldPassword, newPassword} = req.body;
        const user = await User.findById(req.user.userId);
        const currPassword = user['_doc']['password'];

        const validPassword = bcrypt.compareSync(oldPassword, currPassword);
        if (!validPassword) {
            return res.status(400).send({
                message: 'The old password is invalid',
            });
        }

        await User.updateOne({'_id': req.user.userId},
            {password: bcrypt.hashSync(newPassword, 10)});

        res.status(200).send({
            message: 'Password changed successfully',
        });
    } catch (err) {
        res.status(400).send({
            message: `Error: ${err}`,
        });
    }

}
