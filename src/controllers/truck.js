const User = require('../models/User')
const Truck = require('../models/Truck')
const truckSize = require('../helpers/truckTypes')

module.exports.addTruck = async function (req, res) {
    try {
        const driverId = req.user.userId
        const user = await User.findById(driverId)

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'User is not a driver'
            })
        }

        const {type} = req.body
        const [width, length, height, payload] = truckSize(type)

        const truck = new Truck({
            created_by: driverId,
            type,
            width,
            length,
            height,
            payload,
        })

        await truck.save()
        res.status(200).json({
            message: 'Truck created successfully'
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.getTrucks = async function (req, res) {
    try {
        const driverId = req.user.userId
        const user = await User.findById(driverId)
        const trucks = await Truck.find({created_by: driverId})

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'User is not a driver'
            })
        }

        const responseArray = trucks.map((truck) => {
            const doc = truck['_doc']
            return {
                '_id': doc['_id'],
                'created_by': doc['created_by'],
                'assigned_to': doc['assigned_to'],
                'type': doc['type'],
                'status': doc['status'],
                'created_date': doc['createdAt']
            }
        })

        res.status(200).json({
            trucks: await responseArray
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.getTruckById = async function (req, res) {
    try {
        const truckId = req.params.id
        const userId = req.user.userId
        const user = await User.findById(userId)

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'Access is denied. User is not a driver'
            })
        }

        try {
            const truck = await Truck.findById(truckId)
            const doc = truck['_doc']

            res.status(200).json({
                truck: {
                    '_id': doc['_id'],
                    'created_by': doc['created_by'],
                    'assigned_to': doc['assigned_to'],
                    'type': doc['type'],
                    'status': doc['status'],
                    'created_date': doc['createdAt']
                }
            })
        } catch (err) {
            return res.status(400).json({
                message: 'This truck doesn\'t exist'
            })
        }

    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.updateTruckById = async function (req, res) {
    try {
        const truckId = req.params.id
        const driverId = req.user.userId

        const truck = await Truck.findById(truckId)
        if (truck['_doc']['created_by'].toString() !== driverId) {
            return res.status(400).json({
                message: 'Access is denied'
            })
        }

        const onLoadTruck = await Truck.findOne({
            created_by: driverId,
            status: 'OL'
        })

        const assignedTruck = await Truck.findOne({
            _id: truckId,
            assigned_to: driverId
        })

        if (onLoadTruck || assignedTruck) {
            return res.status(400).json({
                message: 'Forbidden to update truck. Truck is assigned or driver is on load'
            })
        } else {
            await Truck.findByIdAndUpdate(truckId, {type: req.body.type})
            res.status(200).json({
                message: 'Truck details changed successfully'
            })
        }
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.deleteTruckById = async function (req, res) {
    try {
        const driverId = req.user.userId

        const truck = await Truck.findById(req.params.id)
        if (truck['_doc']['created_by'].toString() !== driverId) {
            return res.status(400).json({
                message: 'Access is denied'
            })
        }

        const onLoadTruck = await Truck.findOne({
            created_by: driverId,
            status: 'OL'
        })

        const assignedTruck = await Truck.findOne({
            _id: req.params.id,
            assigned_to: driverId
        })

        if (onLoadTruck || assignedTruck) {
            return res.status(400).json({
                message: 'Forbidden to delete truck. Truck is assigned or driver is on load',
            })
        } else {
            await Truck.findByIdAndDelete(req.params.id)

            return res.status(200).json({
                message: 'Truck deleted successfully'
            })
        }
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.assignTruckById = async function (req, res) {
    try {
        const truckId = req.params.id
        const driverId = req.user.userId

        const truck = await Truck.findById(truckId)
        if (truck['_doc']['created_by'].toString() !== driverId) {
            return res.status(400).json({
                message: 'Access is denied'
            })
        }

        await Truck.findByIdAndUpdate(truckId, {assigned_to: driverId})

        res.status(200).json({
            message: 'Truck assigned successfully'
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

