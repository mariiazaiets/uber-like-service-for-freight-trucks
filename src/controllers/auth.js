const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const config = require('../../config')

module.exports.register = async function (req, res) {
    const {email, password, role} = req.body;
    const person = await User.findOne({email})

    if (person) {
        //If the user exists, we throw the error
        return res.status(400).json({
            message: 'User with this email address already exists'
        })
    }

    //Otherwise, we create a user
    const salt = bcrypt.genSaltSync(10)
    const user = new User({
        email,
        password: bcrypt.hashSync(password, salt),
        role,
    })

    try {
        await user.save()
        res.status(200).json({
            message: 'Profile created successfully'
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.login = async function (req, res) {
    try {
        const {email, password} = req.body;
        const user = await User.findOne({email})

        if (!user) {
            res.status(400).json({
                message: 'Invalid username or password'
            })
        }

        //If such a user exists, we check the password
        const validPassword = bcrypt.compareSync(password, user.password)

        if (!validPassword) {
            return res.status(400).send({
                message: 'Invalid username or password',
            });
        }

        //If passwords match - generate JWT token
        const token = jwt.sign({
            email: user.email,
            userId: user.id
        }, config.jwt, {expiresIn: '1h'})
        return res.status(200).send({
            jwt_token: token,
        });

    } catch (err) {
        res.status(400).send({
            message: err,
        });
    }

}

