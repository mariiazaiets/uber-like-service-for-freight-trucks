const Load = require('../models/Load')
const Truck = require('../models/Truck')
const User = require('../models/User')
const findTruck = require('../middleware/load')

module.exports.addLoad = async function (req, res) {
    try {
        const shipperId = req.user.userId;
        const user = await User.findById(shipperId)

        if(!user) {
            return res.status(200).json({
                message: 'User is not found'
            })
        }

        if (user.role !== 'SHIPPER') {
            return res.status(400).json({
                message: 'User is not a shipper'
            })
        }

        const {name, payload, pickup_address, delivery_address, dimensions} = req.body
        const logs = [{
            message: 'Load created',
            time: Date.now()
        }]

        const load = new Load({
            created_by: shipperId,
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions,
            logs
        })

        await load.save()

        res.status(200).json({
            message: 'Load created successfully'
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.getUserLoads = async function (req, res) {
    try {
        let loads
        const userId = req.user.userId
        const user = await User.findById(userId)

        if (!user) {
            return res.status(400).json({
                message: 'User is not found'
            })
        }

        if (user.role === 'DRIVER') {
            loads = await Load.find({assigned_to: req.user.created_by, status: 'SHIPPED'})
        } else {
            loads = await Load.find({created_by: userId})
        }

        const responseLoads = loads.map(load => {
            return {
                '_id': load._id,
                'created_by': load.created_by,
                'assigned_to': load.assigned_to,
                'status': load.status,
                'state': load.state,
                'name': load.name,
                'payload': load.payload,
                'pickup_address': load.pickup_address,
                'delivery_address': load.delivery_address,
                'dimensions': {
                    'width': load.dimensions.width,
                    'length': load.dimensions.length,
                    'height': load.dimensions.height,
                },
                'logs': load.logs.map(log =>  {
                    return {message: log.message, time: log.time}
                }),
                'created_date': load.createdAt
            }
        })

        if (!loads || !responseLoads) {
            return res.status(400).json({
                message: 'You don\'t have any loads yet'
            })
        }

        res.status(200).json({
            loads: responseLoads
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.getActiveLoad = async function (req, res) {
    try {
        const userId = req.user.userId
        const user = await User.findById(userId)
        const load = await Load.findOne({ assigned_to: userId, status: 'ASSIGNED' }, '-__v -logs._id');

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'Access is denied. User is not a driver'
            })
        }

        if (!load) {
            return res.status(400).json({
                load: 'This load doesn\'t exist'
            })
        }

        const outputLoad = {
            '_id': load._id,
            'created_by': load.created_by,
            'assigned_to': load.assigned_to,
            'status': load.status,
            'state': load.state,
            'name': load.name,
            'payload': load.payload,
            'pickup_address': load.pickup_address,
            'delivery_address': load.delivery_address,
            'dimensions': {
                'width': load.dimensions.width,
                'length': load.dimensions.length,
                'height': load.dimensions.height,
            },
            'logs': load.logs.map(log =>  {
                return {message: log.message, time: log.time}
            }),
            'created_date': load.createdAt
        }

        res.status(200).json({
            load: outputLoad
        })

    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.iterateToNextLoadState = async function (req, res) {
    try {
        const userId = req.user.userId
        const user = await User.findById(userId)
        let load = await Load.findOne({ assigned_to: userId, status: { $ne: 'SHIPPED' } })
        let newState

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'Access is denied. User is not a driver'
            })
        }

        if (!load) {
            return res.status(400).json({
                message: 'This load doesn\'t exist'
            })
        }

        switch (load.state) {
            case 'Ready to Pick Up':
                newState = 'En route to Pick Up'
                break
            case 'En route to Pick Up':
                newState = 'Arrived to Pick Up'
                break
            case 'Arrived to Pick Up':
                newState = 'En route to delivery'
                break
            case 'En route to delivery':
                newState = 'Arrived to delivery'
                break
            case 'Arrived to delivery':
                newState = null
                break
            default:
                newState = null
                break
        }

        if (!newState) {
            return res.status(200).json({
                message: 'The load has already arrived to delivery'
            })
        }

        const update = {
            state: newState,
            $push: {
                logs: {
                    message:
                        `Current load state is ${newState}`,
                    time: Date.now()
                }
            }
        }

        if (newState === 'Arrived to delivery') {
            update['status'] = 'SHIPPED'

            await Truck.findOneAndUpdate({created_by: userId}, {status: 'IS'})
        }

        await Load.findByIdAndUpdate(load['_doc']['_id'], update)

        res.status(200).json({
            message: `Load state changed to ${newState}`
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.getLoadById = async function (req, res) {
    try {
        const loadId = req.params.id
        const load = await Load.findById(loadId)

        if (!load) {
            return res.status(400).json({
                message: 'The load doesn\'t exist'
            })
        }

        const responseLoad = {
            '_id': load._id,
            'created_by': load.created_by,
            'assigned_to': load.assigned_to,
            'status': load.status,
            'state': load.state,
            'name': load.name,
            'payload': load.payload,
            'pickup_address': load.pickup_address,
            'delivery_address': load.delivery_address,
            'dimensions': {
                'width': load.dimensions.width,
                'length': load.dimensions.length,
                'height': load.dimensions.height,
            },
            'logs': load.logs.map(log =>  {
                return {message: log.message, time: log.time}
            }),
            'created_date': load.createdAt
        }

        res.status(200).json({
            load: responseLoad
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.updateLoadById = async function (req, res) {
    try {
        const loadId = req.params.id
        const userId = req.user.userId
        const user = await User.findById(userId)

        const load = await Load.findById(loadId)

        if (!load) {

            return res.status(400).json({
                message: 'This load doesn\'t exist'
            })
        }

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'You cannot change this load. User is not a shipper'
            })
        }

        if (load.status !== 'NEW') {
            return res.status(400).json({
                message: 'You cannot change this load. Its status is not NEW'
            })
        }

        const update = {
            name: req.body.name || load.name,
            payload: req.body.payload || load.payload,
            pickup_address: req.body.pickup_address ||
                load.pickup_address,
            delivery_address: req.body.delivery_address ||
                load.delivery_address,
            dimensions: {
                width: req.body.dimensions.width ||
                    load.dimensions.width,
                length: req.body.dimensions.length ||
                    load.dimensions.length,
                height: req.body.dimensions.height ||
                    load.dimensions.height
            }
        }

        await Load.findByIdAndUpdate(loadId, update)

        res.status(200).json({
            message: 'Load details changed successfully',
        });
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.deleteLoadById = async function (req, res) {
    try {
        const loadId = req.params.id;
        const userId = req.user.userId
        const user = await User.findById(userId)

        const load = await Load.findById(loadId)
        if (!load) {

            return res.status(400).json({
                message: 'This load doesn\'t exist'
            })
        }

        if (user.role !== 'DRIVER') {
            return res.status(400).json({
                message: 'You cannot delete this load. User is not a shipper'
            })
        }

        if (load.status !== 'NEW') {
            return res.status(400).json({
                message: 'You cannot delete this load. Its status is not NEW'
            })
        }
        await Load.findByIdAndDelete(loadId);

        res.status(200).json({
            message: 'Load deleted successfully'
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.postLoadById = async function (req, res) {
    try {
        const loadId = req.params.id
        const userId = req.user.userId
        const user = await User.findById(userId)

        const load = await Load.findById(loadId)
        if (!load) {
            return res.status(400).json({
                message: 'This load doesn\'t exist'
            })
        }

        if (user.role !== 'SHIPPER') {
            return res.status(400).json({
                message: 'Access is denied. User is not a shipper'
            })
        }

        if (load.status !== 'NEW') {
            return res.status(400).json({
                message: 'You cannot post this load. Its status is not NEW'
            })
        }

        await Load.findByIdAndUpdate(loadId, {status: 'POSTED'})

        const truckState = findTruck(loadId)

        truckState
            .then((truck) => {
            if (truck.state === 'No suitable truck') {
                return res.status(200).json({
                    message: 'No drivers found',
                    driver_found: true
                })
            } else if (truck.state === 'En route to Pick Up') {
                return res.status(200).json({
                    message: 'Load posted successfully',
                    driver_found: true
                })
            } else {
                return res.status(400).json({
                    message: 'Something went wrong. Try restarting.'
                })
            }
        })
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

module.exports.getLoadShippingInfoById = async function (req, res) {
    try {
        const userId = req.user.userId
        const user = await User.findById(userId)
        const loadId = req.params.id
        let truck

        try{
            const load = await Load.findOne({ _id: loadId, created_by: req.user.created_by, status: { $nin: ['NEW', 'SHIPPED'] } }, '-__v -logs._id')
            truck = await Truck.findOne({created_by: load.assigned_to, status: 'OL'}, '-__v');

            if (user.role !== 'DRIVER') {
                return res.status(400).json({
                    message: 'Access is denied. User is not a shipper'
                })
            }

            const responseLoad = {
                '_id': load._id,
                'created_by': load.created_by,
                'assigned_to': load.assigned_to,
                'status': load.status,
                'state': load.state,
                'name': load.name,
                'payload': load.payload,
                'pickup_address': load.pickup_address,
                'delivery_address': load.delivery_address,
                'dimensions': {
                    'width': load.dimensions.width,
                    'length': load.dimensions.length,
                    'height': load.dimensions.height,
                },
                'logs': load.logs.map(log =>  {
                    return {message: log.message, time: log.time}
                }),
                'created_date': load.createdAt
            }

            const responseTruck = {
                '_id': truck._id,
                'created_by': truck.created_by,
                'assigned_to': truck.assigned_to,
                'type': truck.type,
                'status': truck.status,
                'created_date': truck.createdAt
            }

            res.status(200).json({
                load: responseLoad,
                truck: responseTruck
            })
        } catch (err) {
            return res.status(400).json({
                message: 'This load doesn\'t exist'
            })
        }
    } catch (err) {
        res.status(500).json({
            message: err
        })
    }
}

