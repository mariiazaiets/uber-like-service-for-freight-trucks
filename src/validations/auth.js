const Joi = require('joi')

module.exports = {
    registerForm: Joi.object().keys({
        email: Joi.string().email({minDomainSegments: 2}),
        password: Joi.string()
            .regex(/^[a-zA-Z0-9-_]{6,128}$/)
            .min(6)
            .max(128)
            .required(),
        role: Joi.string().insensitive().valid('driver', 'shipper').required()
    }),
    loginForm: Joi.object().keys({
        email: Joi.string().email({minDomainSegments: 2}),
        password: Joi.string().min(6).max(128).required()
    })
}