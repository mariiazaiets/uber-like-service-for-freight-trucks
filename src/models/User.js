const {Schema, model} = require('mongoose');

const UserSchema = new Schema({
    email: {type: String, unique: true},
    password: {type: String, required: true},
    role: {type: String, enum: ['DRIVER', 'SHIPPER']},
    //next line for role, but terminal throw error with it
    // required: true
}, {timestamps: true});

module.exports = model('User', UserSchema);