const {Schema, model, Types} = require('mongoose');

const TruckSchema = new Schema({
    created_by: {type: Types.ObjectId, required: true, ref: 'User'},
    assigned_to: {type: Types.ObjectId, ref: 'User', default: null},
    type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    },
    status: {type: String, enum: ['IS', 'OL'], default: 'IS'},
    width: {type: Number, required: true, min: 0, max: 1000},
    length: {type: Number, required: true, min: 0, max: 1000},
    height: {type: Number, required: true, min: 0, max: 1000},
    payload: {type: Number, required: true, min: 0, max: 5000},
}, {timestamps: true});

module.exports = model('Truck', TruckSchema);