const {Schema, model, Types} = require('mongoose');

const LoadSchema = new Schema({
    created_by: {type: Types.ObjectId, required: true, ref: 'User'},
    assigned_to: {type: Types.ObjectId, ref: 'User', default: null},
    status: {
        type: String,
        required: true,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        default: 'NEW',
    },
    state: {
        type: String,
        enum: [
            'Ready to Pick Up',
            'En route to Pick Up',
            'Arrived to Pick Up',
            'En route to delivery',
            'Arrived to delivery',
        ],
        default: 'Ready to Pick Up',
    },
    name: {type: String, required: true, default: 'Load'},
    payload: {type: Number, required: true, min: 0, max: 5000},
    pickup_address: {type: String},
    delivery_address: {type: String},
    dimensions: {
        width: {type: Number, required: true, min: 0, max: 1000},
        length: {type: Number, required: true, min: 0, max: 1000},
        height: {type: Number, required: true, min: 0, max: 1000},
    },
    logs: [{
        message: {type: String, required: true},
        time: {type: Date, default: Date.now},
    }],
}, {timestamps: true});

module.exports = model('Load', LoadSchema);