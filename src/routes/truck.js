const express = require('express')
const router = express.Router()
const controller = require('../controllers/truck')

router.get('/', controller.getTrucks)
router.post('/', controller.addTruck)
router.get('/:id', controller.getTruckById)
router.put('/:id', controller.updateTruckById)
router.delete('/:id', controller.deleteTruckById)
router.post('/:id/assign', controller.assignTruckById)

module.exports = router