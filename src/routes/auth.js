const express = require('express')
const router = express.Router()
const controller = require('../controllers/auth')
const schemas = require('../validations/auth')
const validator = require('../middleware/schema')

router.post('/login', validator(schemas.loginForm, 'body'), controller.login)
router.post('/register', validator(schemas.registerForm, 'body'), controller.register)

module.exports = router