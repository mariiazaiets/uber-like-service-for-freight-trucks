const express = require('express')
const router = express.Router()
const controller = require('../controllers/user')

router.get('/me', controller.getProfileInfo)
router.delete('/me', controller.deleteUserProfile)
router.patch('/me/password', controller.changeUserPassword)

module.exports = router