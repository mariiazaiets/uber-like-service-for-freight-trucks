const express = require('express')
const router = express.Router()
const controller = require('../controllers/load')

router.get('/', controller.getUserLoads)
router.post('/', controller.addLoad)
router.get('/active', controller.getActiveLoad)
router.patch('/active/state', controller.iterateToNextLoadState)
router.get('/:id', controller.getLoadById)
router.put('/:id', controller.updateLoadById)
router.delete('/:id', controller.deleteLoadById)
router.post('/:id/post', controller.postLoadById)
router.get('/:id/shipping_info', controller.getLoadShippingInfoById)



module.exports = router